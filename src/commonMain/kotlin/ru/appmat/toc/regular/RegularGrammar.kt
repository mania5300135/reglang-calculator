package ru.appmat.toc.regular

enum class Orientation {
    Right, Left
}

sealed class RegularGrammarRule {
    object EmptyRule : RegularGrammarRule() {
        override fun toString(): String = LAMBDA.toString()
    }

    class LetterRule(val letter: Char) : RegularGrammarRule() {
        override fun toString() = "$letter"
    }

    sealed class NonTerminalRule(val letter: Char, val nonTerminal: Char, val orientation: Orientation) :
        RegularGrammarRule() {
        override fun toString() = if (orientation == Orientation.Right) {
            "$letter$nonTerminal"
        } else {
            "$nonTerminal$letter"
        }

        class RightRule(letter: Char, nonTerminal: Char) : NonTerminalRule(letter, nonTerminal, Orientation.Right)

        class LeftRule(letter: Char, nonTerminal: Char) : NonTerminalRule(letter, nonTerminal, Orientation.Left)
    }
}


sealed class RegularGrammar<T : Regular<T>>(
    val startVariable: Char,
    rules: Map<Char, Set<RegularGrammarRule>>,
    val orientation: Orientation
) : Regular<T> {

    init {
        rules.values.flatten().all { r ->
            when (r) {
                is RegularGrammarRule.NonTerminalRule -> r.orientation == orientation
                else -> true
            }
        }
    }

    abstract fun toLambdaFree(): T

    abstract fun withRule(rule: RegularGrammarRule): T


    class LeftRegularGrammar(
        startVariable: Char,
        rules: Map<Char, Set<RegularGrammarRule>>
    ) :
        RegularGrammar<LeftRegularGrammar>(startVariable, rules, Orientation.Left) {
        /**
         * Задача:
         * звезда Клини
         */
        //region left-grammar-closure
        override fun starClosure(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun plusClosure(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }
        //endregion

        /**
         * Задача:
         * конкатенация
         * Исполнитель [А-05-17] Окованцев Павел
         */
        override fun concatenate(other: LeftRegularGrammar): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun intersect(other: LeftRegularGrammar): LeftRegularGrammar {
            TODO("Not yet implemented")
        }


        override fun complement(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * объединение
         */
        override fun union(other: LeftRegularGrammar): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * грамматика для реверса языка
         */
        override fun reversed(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun withRule(rule: RegularGrammarRule): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * КЯК
         */
        override fun check(word: Word): Boolean {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразование к НКА
         */
        fun toNFA(): FiniteAutomaton.NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразование к правой грамматике
         */
        fun toRight(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Избавиться от всех lambda, кроме S -> lambda
         */
        override fun toLambdaFree(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

    }

    class RightRegularGrammar(
        startVariable: Char,
        rules: Map<Char, Set<RegularGrammarRule>>
    ) : RegularGrammar<RightRegularGrammar>(
        startVariable,
        rules,
        Orientation.Right
    ) {
        /**
         * Задача:
         * звезда Клини
         */
        //region right-grammar-closure
        override fun starClosure(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun plusClosure(): RightRegularGrammar {
            TODO("Not yet implemented")
        }
        //endregion

        /**
         * Задача:
         * конкатенация
         */
        override fun concatenate(other: RightRegularGrammar): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun intersect(other: RightRegularGrammar): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun complement(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * объединение
         */
        override fun union(other: RightRegularGrammar): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * грамматика для реверса языка
         */
        override fun reversed(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun withRule(rule: RegularGrammarRule): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * КЯК
         */
        override fun check(word: Word): Boolean {
            TODO("Not yet implemented")
        }


        /**
         * Задача:
         * Преобразование к НКА
         */
        fun toNFA(): FiniteAutomaton.NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразование к левой грамматике
         */
        fun toLeft(): LeftRegularGrammar {
            TODO("Not yet implemented")

        }

        fun toRegExp(): RegularExpression = toNFA().toRegExp()
        fun toDFA(): FiniteAutomaton.DFA = toNFA().toDFA()

        /**
         * Задача:
         * Избавиться от всех lambda, кроме S -> lambda
         */
        override fun toLambdaFree(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

    }

}

