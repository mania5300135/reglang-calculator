package ru.appmat.toc.regular

typealias Letter = Char
typealias CharAlphabet = Set<Letter>
typealias Word = String

const val LAMBDA: Char = '\u03BB'

interface Regular<T : Regular<T>> {
    fun starClosure(): T
    fun plusClosure(): T
    fun concatenate(other: T): T
    fun intersect(other: T): T
    fun complement(): T
    fun union(other: T): T
    fun reversed(): T
    fun check(word: Word): Boolean

}