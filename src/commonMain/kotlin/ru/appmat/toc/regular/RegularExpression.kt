package ru.appmat.toc.regular

/**
 * Задача:
 * реализовать сам класс как аналог AST и функции из regex-basic
 */
class RegularExpression :
    Regular<RegularExpression> {

    enum class RegExpToNFAConversionAlgorithm {
        Thompson,
        Glushkov
    }

    //region regex-basic
    override fun concatenate(other: RegularExpression): RegularExpression {
        TODO("Not yet implemented")
    }

    override fun intersect(other: RegularExpression): RegularExpression {
        TODO("Not yet implemented")
    }

    override fun union(other: RegularExpression): RegularExpression {
        TODO("Not yet implemented")
    }

    override fun plusClosure(): RegularExpression {
        TODO("Not yet implemented")
    }

    override fun starClosure(): RegularExpression {
        TODO("Not yet implemented")
    }
    //endregion regex-basic

    /**
     * Задача:
     * дополнение к языку и его реверс в виде регулярки
     */
    //region regex-complement
    override fun complement(): RegularExpression {
        TODO("Not yet implemented")
    }

    override fun reversed(): RegularExpression {
        TODO("Not yet implemented")
    }
    //endregion

    override fun check(word: Word): Boolean {
        return this.toNFA().check(word);
    }

    fun toNFA(algorithm: RegExpToNFAConversionAlgorithm = RegExpToNFAConversionAlgorithm.Thompson): FiniteAutomaton.NFA {
        return when (algorithm) {
            RegExpToNFAConversionAlgorithm.Thompson -> thompsonConversion()
            RegExpToNFAConversionAlgorithm.Glushkov -> glushkovConversion()
        }
    }

    /**
     * Задача:
     * Приведение к правой регулярной грамматике
     */
    fun toRightGrammar(): RegularGrammar.RightRegularGrammar {
        TODO("Not yet implemented")
    }


    /**
     * Задача:
     * Приведение к левой регулярной грамматике
     * Исполнитель: [А-13-17] Alieva Irada
     */
    fun toLeftGrammar(): RegularGrammar.LeftRegularGrammar {
        TODO("Not yet implemented")
    }

    fun toDFA() = toNFA().toDFA()

    /**
     * Задача:
     * алгоритм Глушкова @see https://en.wikipedia.org/wiki/Glushkov%27s_construction_algorithm
     */
    private fun glushkovConversion(): FiniteAutomaton.NFA {
        TODO("Not yet implemented")
    }

    /**
     * Задача:
     * алгоритм Томпсона @see https://en.wikipedia.org/wiki/Thompson%27s_construction
     */
    private fun thompsonConversion(): FiniteAutomaton.NFA {
        TODO("Not yet implemented")
    }


}