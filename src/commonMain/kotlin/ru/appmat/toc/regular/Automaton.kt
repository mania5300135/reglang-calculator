package ru.appmat.toc.regular

typealias NondeterministicFiniteAutomaton = FiniteAutomaton.NFA
typealias DeterministicFiniteAutomaton = FiniteAutomaton.DFA

sealed class FiniteAutomaton<AutomatonType : FiniteAutomaton<AutomatonType>>(
    val stateCount: Int
) : Regular<AutomatonType> {


    /**
     * Задача
     * Описать структуру данных ДКА и реализовать dfa-basic
     */
    class DFA(stateCount: Int, val alphabet: CharAlphabet /*TODO*/) :
        FiniteAutomaton<DFA>(stateCount) {
        //region dfa-basic
        override fun check(word: Word): Boolean {
            TODO("Not yet implemented")
        }

        /**
         * Должна расширять алфавит текущего ДКА
         */
        fun withAlphabet(alphabet: CharAlphabet): DFA {
            TODO("Not yet implemented")
        }

        override fun complement(): DFA {
            TODO("Not yet implemented")
        }

        //endregion dfa-basic

        /**
         * Задача:
         * звезда Клини
         *	Исполнитель: [А-05-17] Павлов Иван
         */
        //region dfa-closure
        override fun starClosure(): DFA {
            TODO("Not yet implemented")
        }

        override fun plusClosure(): DFA {
            TODO("Not yet implemented")
        }
        //endregion dfa-closure


        /**
         * Задача:
         * конкатенация и преобразование в НКА
         */
        //region dfa-concatenation
        override fun concatenate(other: DFA): DFA {
            TODO("Not yet implemented")
        }

        private fun toNFA(): NFA {
            TODO("Not yet implemented")
        }
        //endregion


        /**
         * Задача:
         * пересечение и объединение
         * Исполнитель: [A-13-17] Худяков Константин
         */
        //region dfa-intersection
        override fun intersect(other: DFA): DFA {
            TODO("Not yet implemented")
        }

        override fun union(other: DFA): DFA {
            TODO("Not yet implemented")
        }
        //endregion

        /**
         * Задача:
         * ДКА, принимающий реверсию всех слов языка текущего ДКА
         */
        override fun reversed(): DFA {
            TODO("Not yet implemented")
        }

        enum class DFAMinimizationAlgorithm {
            Hopcroft, Moore, Brzozowski
        }

        fun minimized(algorithm: DFAMinimizationAlgorithm = DFAMinimizationAlgorithm.Hopcroft): DFA {
            return when (algorithm) {
                DFAMinimizationAlgorithm.Hopcroft -> hopcroftMinimized()
                DFAMinimizationAlgorithm.Moore -> mooreMinimized()
                DFAMinimizationAlgorithm.Brzozowski -> brzozowskiMinimized()
            }
        }

        /**
         * Задача:
         * алгоритм Брзощовски
         */
        private fun brzozowskiMinimized(): DFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * алгоритм Мура
         * Исполнитель: [А-13-17] Новиков Александр
         */
        private fun mooreMinimized(): DFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * алгоритм Хопкрофта
         */
        private fun hopcroftMinimized(): DFA {
            TODO("Not yet implemented")
        }

        fun toRightGrammar(): RegularGrammar.RightRegularGrammar = this.toNFA().toRightGrammar()

        fun toLeftGrammar(): RegularGrammar.LeftRegularGrammar = this.toNFA().toLeftGrammar()
    }


    /**
     * Задача:
     * реализовать NFA и nfa-basic
     */
    class NFA(stateCount: Int /*TODO*/) :
        FiniteAutomaton<NFA>(stateCount) {

        companion object {
            /**
             * Задача:
             * генератор случайного НКА в подалфавите [a-z]
             * @param alphabetSize – размер целевого алфавита
             * @param hasLambda – допустимо ли использовать лямбда-переходы
             */
            fun generate(alphabetSize: Int, hasLambda: Boolean = true): NFA {
                TODO("Not yet implemented")
            }
        }

        //region nfa-basic
        override fun check(word: Word): Boolean {
            TODO("Not yet implemented")
        }

        override fun concatenate(other: NFA): NFA {
            TODO("Not yet implemented")
        }
        //endregion nfa-basic

        /**
         * Задача:
         * НКА, принимающий дополнение языка текущего НКА
         * и убрать тупики
         */
        override fun complement(): NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразовать к НКА без лямбда-переходов
         */
        fun removeLambdaTransitions(): NFA {
            TODO("Not yet implemented")
        }


        /**
         * Задача:
         * пересечение и объединение НКА
         */
        //region nfa-intersection
        override fun intersect(other: NFA): NFA {
            TODO("Not yet implemented")
        }

        override fun union(other: NFA): NFA {
            TODO("Not yet implemented")
        }
        //endregion

        /**
         * Задача:
         * НКА, принимающий реверсию всех слов языка текущего НКА
         * Ограничение: единственное начальное состояние
         */
        override fun reversed(): NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * звезда Клини
         */
        //region nfa-closure
        override fun starClosure(): NFA {
            TODO("Not yet implemented")
        }

        override fun plusClosure(): NFA {
            TODO("Not yet implemented")
        }
        //endregion


        enum class NFAToRegExpConversionAlgorithm {
            Kleene,
            StateElimination
        }

        fun toRegExp(algorithm: NFAToRegExpConversionAlgorithm = NFAToRegExpConversionAlgorithm.StateElimination): RegularExpression {
            return when (algorithm) {
                NFAToRegExpConversionAlgorithm.Kleene -> kleeneConversion()
                NFAToRegExpConversionAlgorithm.StateElimination -> stateElimination()
            }
        }

        /**
         * Задача:
         * Метод удаления состояний
         */
        private fun stateElimination(): RegularExpression {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * алгоритм Клини
         * @see https://en.wikipedia.org/wiki/Kleene%27s_algorithm
         */
        private fun kleeneConversion(): RegularExpression {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * алгоритм построения подмножеств
         */
        fun toDFA(): DFA {
            TODO("Not yet implemented")

        }


        /**
         * Задача:
         * Приведение к правой регулярной грамматике
         */
        fun toRightGrammar(): RegularGrammar.RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Приведение к левой регулярной грамматике
         */
        fun toLeftGrammar(): RegularGrammar.LeftRegularGrammar {
            TODO("Not yet implemented")
        }

    }
}


/**
 * Задача
 * Алгоритм Ахо-Корасика
 *  @see https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm
 *	Исполнитель: [А-13-17] Полянский Родион
 */
fun achoCorasik(/*TODO*/) {
    TODO("Not yet implemented")
}

/**
 * Задача
 * Алгоритм Бертранда, альтернатива Ахо-Карасику
 * @see http://se.ethz.ch/~meyer/publications/string/string_matching.pdf
 */
fun bertrand(/*TODO*/) {
    TODO("Not yet implemented")
}