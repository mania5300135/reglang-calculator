package sample

expect class Sample() {
    fun checkMe(): Int
}

expect object Platform {
    val name: String
}

class CommonTestable {
    fun helloCommon() = "Now you can test me in common module"
}

fun hello(): String = "Hello from ${Platform.name}"

